--------------------------------------------------------------------------------
-- the processor status is a device
-- contains alu condition codes and interrupt control
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.fable.all;
use work.fabio.all;

entity ccreg is
port(	clk: in std_logic;
	rst: in std_logic;			-- reset
	rdata: out alucc;			-- current CCs
	isr: in std_logic;			-- executing interrupt
	ccw: in std_logic;			-- write CCs
	wdata: in alucc);			-- updated CCs
end entity;

architecture syn of ccreg is
	signal cc0: alucc := (others=>'0');	-- normal CCs
	signal cc1: alucc := (others=>'0');	-- interrupt CCs
begin
	-- selected CCs
	rdata <= cc1 when isr='1' else cc0;

	-- handle CC writes
	process
	begin
		wait until rising_edge(clk);
		if rst='1' then
			cc0 <= (others=>'0');
			cc1 <= (others=>'0');
		elsif ccw='1' then
			report ">cc"&ht&to_string(wdata)&cr;
			case isr is
			when '0' =>
				cc0 <= wdata;
			when '1' =>
				cc1 <= wdata;
			when others => null;
			end case;
		end if;
	end process;
end syn;

