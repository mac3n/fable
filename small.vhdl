--------------------------------------------------------------------------------
-- minimal oontroller
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;
use work.fabio.all;
use work.code.all;

entity small is
generic(divisor: natural := 5);			-- default clock divisor
port(	ctl: in ioctl;
	cmd: out iocmd;
        rest: in iorsp);
end entity;

architecture syn of small is

	-- aggregated bus signals
	constant slots: natural := 2;
	signal scmd: iocmd;			-- broadcast command
	signal ios: iolist(0 to slots);		-- chained responses
	
begin

	cmd <= scmd;				-- export bus
	-- core cpu & devs
CP:	component cpu
		port map(ctl=>ctl, cmd=>scmd, rest=>ios(0));
	-- rom
RO:	component rom
		generic map(ma=>X"0000", addrwidth=>7, data=>code)
		port map(ctl=>ctl, cmd=>scmd, rsp=>ios(0), rest=>ios(1));
	-- ram
RA:	component ram
		generic map(ma=>X"0100", addrwidth=>10)
		port map(ctl=>ctl, cmd=>scmd, rsp=>ios(1), rest=>ios(2));
	-- end of bus
	ios(slots) <= rest;
end syn;

