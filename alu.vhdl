--------------------------------------------------------------------------------
-- arithmetic and logical ops
-- the LHS can be shifted or byte-swapped
-- the addrer can take carry-in
-- produces 16 bit result and MCZ condition codes
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;

entity alu is
port(	clk: in std_logic;
	a: in std_logic;			-- enable
	op: in aluop;	-- operation
	lhs, rhs: in word;			-- accumulator, source operands
	ci: in std_logic;			-- carry-in
	res: out word;				-- alu result
	cc: out alucc);				-- flags
end alu;

architecture syn of alu is
	signal xsum: std_logic_vector(16 downto 0);	-- extended add/sub
	signal xlhs: word; 			-- operands
	signal xci: std_logic;			-- carry-in

	signal acc: word;			-- normal result
	signal mcz: alucc;			-- conditions

	signal add, sub: std_logic := '0';	-- adds

	component addsubc is
	port(	add, sub: in std_logic;			-- enables
		a, b: in word;				-- operands
		c: in std_logic;			-- carry in
		s: out std_logic_vector(16 downto 0));	-- extended sums
	end component;
begin

	res <= acc;					-- output word
	cc <= mcz;					-- output conditions

	-- extended adder operands

	-- LHS accumulator operand
	process(a,op,lhs)
	begin
		xlhs <= lhs;
		if a='1' then				-- alu enable
			case op is
			when "1101" =>			-- shift right
				xlhs <= "0"&lhs(15 downto 1);
			when "1100" =>			-- byte swap
				xlhs <= lhs(7 downto 0)&lhs(15 downto 8);
			when others => null;
			end case;
		end if;
	end process;

	-- carry-in
	process(a,op,ci)
	begin
		xci <= '0';
		if a='1' then
			case op is
			when "1110"|"1111" =>		-- add/sub with carry
				xci <= ci;		-- with carry
			when others => null;
			end case;
		end if;
	end process;

	-- extended add/subtract
S:	component addsubc
		port map(add=>add, sub=>sub,
			a=>xlhs, b=>rhs, c=>xci, s=>xsum);

	-- select result and conditions
	process(a,op,lhs,rhs,ci,xlhs,xsum,acc,mcz)
	begin
		acc <= (others=>'X');			-- don't care
		mcz <= (others=>'X');
		add <= '0';
		sub <= '0';
		if a='1' then				-- alu enable
			acc <= xsum(15 downto 0);	-- word sum
			mcz(1) <= ci;			-- carry-in
			case op is
			when "0000"|"1000" =>		-- load/test
				acc <= rhs;
			when "0001"|"1001" =>		-- logical and/test and
				acc <= lhs and rhs;
			when "0010"|"1010" =>		-- arithmetic/test add
				add <= '1';
				mcz(1) <= xsum(16);	-- carry
			when "0011"|"1011" =>		-- arithmetic/test subtract
				sub <= '1';
				mcz(1) <= xsum(16);	-- carry out
			when "0100" => acc <= lhs xor rhs;	-- logical xor
			when "0101" =>			-- left rotate
				acc <= lhs(14 downto 0)&lhs(15);
				mcz(1) <= lhs(15);	-- copy into carry ind
			when "0110" => acc <= lhs or rhs;	-- logical or
			when "0111" => acc <= not rhs;	-- load complement
			when "1100" =>			-- byte swap with or
				acc <= xlhs(15 downto 0) or rhs;
			when "1101" =>			-- shift right with add
				add <= '1';
				mcz(1) <= lhs(0);	-- carry from shiftout
			when "1110" =>			-- add with carry
				add <= '1';
				mcz(1) <= xsum(16);	-- carry
			when "1111" =>			-- subtract with carry
				sub <= '1';
				mcz(1) <= xsum(16);	-- carry
			when others => null;
			end case;
			if acc=X"0000" then		-- zero CC
				mcz(0) <= '1';
			else
				mcz(0) <= '0';
			end if;
			mcz(2) <= acc(15);		-- minus CC
		end if;
	end process;

	-- trace ALU activity
	process
	begin
		wait until rising_edge(clk);
		if a='1' then
			report "alu"&to_string(op)&ht
				&to_string(acc)&"("&to_string(mcz)&")"
				&ht&to_string(lhs)&ht&to_string(rhs)&cr;
		end if;
	end process;

end syn;

--------------------------------------------------------------------------------
-- 16-bit add/sub with carry-in/-out
-- This is not always inferred from addition.
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;

entity addsubc is
port(	add, sub: in std_logic;			-- enables
	a, b: in word;				-- operands
	c: in std_logic;			-- carry in
	s: out std_logic_vector(16 downto 0));	-- extended sums
end addsubc;

architecture syn of addsubc is
	signal xa, xb: std_logic_vector(16 downto 0) := (others=>'0');
	signal xc: std_logic_vector(0 downto 0);
	signal xs: unsigned(16 downto 0);
begin
	-- extend operands
	xa(15 downto 0) <= a;
	xb(15 downto 0) <= not b when sub='1' else b;
	xc(0) <= not c when sub='1' else c;

	s <= std_logic_vector(xs);
	process(add,sub,xa,xb,xc)
	begin
		xs <= (others=>'X');
		if add='1' or sub='1' then
			xs <= unsigned(xa)+unsigned(xb)+unsigned(xc);
		end if;
	end process;
end syn;

