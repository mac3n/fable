--------------------------------------------------------------------------------
-- io signal interface
-- no interrupts
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity gpio is
generic(da: daddr := (others=>'0');		-- base address
	x0: word := X"0000");			-- initial state
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp;				-- continue
	-- external I/O signals
	tx: out word;				-- tx/rx lines
	rx: in word);
end entity;

architecture syn of gpio is
	signal clk, rst: std_logic;
	signal s, w, r: std_logic := '0';	-- selected
	signal xdata, rdata: word := x0;	-- registers
begin

	clk <= ctl.clk;
	rst <= ctl.rst;

	-- device selected
SEL:	component iodevs			-- device select
		generic map(da=>da)		-- single address
		port map(cmd=>cmd, s=>s, w=>w, r=>r);
	tx <= xdata;				-- output from register

	-- acknowledge
	rdata <= rx;				-- read from input
TAP:	component iolink			-- bus tap
		port map(ack=>s, rdata=>rdata, rest=>rest, rsp=>rsp);

	process
	begin
		wait until rising_edge(clk);
		if rst='1' then			-- reset
			xdata <= X"0000";
		elsif w='1' then		-- write
			xdata<= cmd.wdata;
			report ">gpio"&to_string(da)&ht&to_string(cmd.wdata)&cr;
		elsif r='1' then		-- read
			report "<gpio"&to_string(da)&ht&to_string(rdata)&cr;		
		end if;
	end process;

end syn;
