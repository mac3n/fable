--------------------------------------------------------------------------------
-- glue a cpu together
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.fable.all;
use work.fabio.all;

entity cpu is
port(	ctl: in ioctl;				-- clocking
	cmd: out iocmd;				-- output
	rest: in iorsp;				-- input
	-- CPU control/status
	hold: in std_logic := '0';		-- hold
	isr: out std_logic;			-- interrupted
	irq: in std_logic := '0');		-- interrupting
end entity;

architecture syn of cpu is
	signal clk, rst: std_logic;		-- global

	signal da: daddr;			-- internal signals
	signal ma: maddr;
	signal d, m, r, w: std_logic := '0';
	signal rdata, wdata: word;		-- src/dst data
	signal dr, dw: std_logic;		-- device access

	signal stall: std_logic;		-- bus wait

	signal ra: raddr;			-- register number
	signal rinc, rdec, rw: std_logic;	-- register ops
	signal rwdata, rrdata: word;		-- register data

	signal a: std_logic;			-- alu control
	signal aop: aluop;
	signal ar, ac: std_logic;		-- alu REG/CC writeback
	signal adata: word;			-- ALU result
	signal acc: alucc;			-- ALU CCs
	signal rcc: alucc;			-- registered CCs

	signal risr: std_logic;			-- current state
begin
	-- clocking in
	clk <= ctl.clk;
	rst <= ctl.rst;

	-- external bus
	cmd.r <= r;				-- reflect r/w state
	cmd.w <= w;
	cmd.d <= d;
	cmd.da <= da;
	cmd.m <= m;
	cmd.ma <= ma;
	cmd.wdata <= wdata;

	isr <= risr;

	-- synchronization
	rdata <= rest.rdata when rest.ack='1' else rrdata;	-- bus or regs
	stall <= hold or ((m or d) and not rest.ack);		-- bus wait

	-- register file
	ma <= rrdata;				-- ma comes from regs
	rwdata <= adata when ar='1' else wdata;	-- reg from bus or alu

RG:	component regs
		port map(clk=>clk,
			ra=>ra, inc=>rinc, dec=>rdec, w=>rw,
			wdata=>rwdata, rdata=>rrdata);

	-- alu
AL:	component alu
		port map(clk=>clk, a=>a, op=>aop,
			lhs=>rrdata, rhs=>wdata, ci=>rcc(1),
			res=>adata, cc=>acc);

	-- condition codes
CC:	component ccreg
		port map(clk=>clk, rst=>rst,
			rdata=>rcc, isr=>risr, ccw=>ac, wdata=>acc);

	-- decoder & sequencer
EX:	component exec
		port map(clk=>clk, rst=>rst, hold=>stall,
			d=>d, m=>m, r=>r, w=>w, da=>da,
			ra=>ra, rinc=>rinc, rdec=>rdec, rw=>rw,
			rdata=>rdata, wdata=>wdata,
			irq=>irq, isr=>risr,
			a=>a, aop=>aop, ar=>ar, ac=>ac, cc=>rcc);
	
	-- i/o trace
	process
	begin
		wait until rising_edge(clk);
		if rest.ack='0' then
			null;
		elsif (d and r)='1' then
			report "<d"&to_string(da)&ht&to_string(rdata)&cr;
		elsif (d and w)='1' then
			report ">d"&to_string(da)&ht&to_string(wdata)&cr;
		elsif (m and r)='1' then
			report "<m"&to_string(ma)&ht&to_string(rdata)&cr;
		elsif (m and w)='1' then
			report ">m"&to_string(ma)&ht&to_string(wdata)&cr;
		end if;
	end process;

	-- alu trace
	process
	begin
		wait until rising_edge(clk);
		if ar='1' then
			report "alu out"&cr;
		end if;
	end process;
end syn;

