-------------------------------------------------------------------------------
-- FPGA Able  dev/mem bus utilities
-- unlike the Able's, this is clocked,
-- it does require an explicit acknowledgement for read/write
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;

package fabio is

	type ioctl is record				-- global system signals
		clk: std_logic;				-- system clock
		rst: std_logic;				-- system reset
	end record;
	type iocmd is record				-- cpu command
		da: daddr;				-- device address
		ma: maddr;				-- memory address
		d, m, r, w: std_logic;			-- bus control
		wdata: word;				-- output data
	end record;
	type iorsp is record				-- dev/mem response
		intr: std_logic;			-- post interrupt
		rdata: word;				-- input data
		ack: std_logic;				-- bus ready
	end record;
	type iolist is array(natural range <>) of iorsp;	-- list of dev/mem

	-- processor
	component cpu is
	port(	ctl: in ioctl;				-- clocking
		cmd: out iocmd;				-- output
		rest: in iorsp;				-- input
		-- CPU control/status
		hold: in std_logic := '0';		-- hold
		isr: out std_logic;			-- interrupted
		irq: in std_logic := '0');		-- interrupting
	end component;

	-- prototype word device
	component dev is
	generic(da: daddr := (others=>'0'));		-- base address
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp);			-- continue bus
	end component;

	-- prototype word memory
	component mem is
	generic(ma: maddr := (others=>'0'));		-- base address
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp);			-- continue
	end component;

	-- prototype rom with initial code
	component rom is
	generic(ma: maddr := (others=>'0');		-- base address
		addrwidth: natural := 8;		-- addresses
		data: wordlist := (0 to 255=>X"0000"));	-- contents
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp);			-- continue
	end component;

	-- prototype ram
	component ram is
	generic(ma: maddr := (others=>'0');		-- base address
		addrwidth: natural := 8);	-- contents
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp);			-- continue
	end component;

	-- interrupt control
	component icw is
	generic(da: daddr := "0000001");		-- device 1
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp; 			-- continue devices
		-- interrupt control
		isr: in std_logic;			-- executing interrupt
		irq: out std_logic := '0');		-- signalling interupt
	end component;

	-- registered i/o bits
	component gpio is
	generic(da: daddr := (others=>'0');		-- base address
		x0: word := X"0000");			-- initial state
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp;				-- continue
		-- external I/O signals
		tx: out word;				-- tx/rx lines
		rx: in word);
	end component;

	-- tick and click
	component click is
	generic(da: daddr := (others=>'0');		-- base address
		divisor: natural := 50_000);		-- default divisor
	port(	ctl: in ioctl;				-- clocking
		cmd: in iocmd;				-- command
		rsp: out iorsp;				-- output
		rest: in iorsp);			-- continue
	end component;

	-- command selections
	component iodevs is
	generic(da: daddr := (others=>'0');		-- base address
		addrwidth: natural := 0);		-- range bits
	port(	cmd: in iocmd;				-- command
		s, r, w: out std_logic);		-- selected command
	end component;

	component iomems is
	generic(ma: maddr := (others=>'0');		-- base address
		addrwidth: natural := maddr'length);	-- range bits
	port(	cmd: in iocmd;				-- command
		s, r, w: out std_logic);		-- selected command
	end component;

	-- response connection
	component iolink is
	port(	intr: in std_logic := '0';		-- propagate intr
		ack: in std_logic;			-- propagate ack
		rdata: in word;				-- select data
		rest: in iorsp;				-- other peripherals
		rsp: out iorsp);			-- to next
	end component;
end package;

-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;
use work.fabio.all;

-- response connection
entity iolink is
port(	intr: in std_logic := '0';		-- propagate intr
	ack: in std_logic;			-- propagate ack
	rdata: in word;				-- select data
	rest: in iorsp;				-- other peripherals
	rsp: out iorsp);			-- to next
end entity;

architecture syn of iolink is
begin
	rsp.intr <= intr or rest.intr;
	rsp.rdata <= rdata when ack='1' else rest.rdata;
	rsp.ack <= ack or rest.ack;
end syn;


-------------------------------------------------------------------------------
-- select a range of device addresses
-- default is a single address
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;
use work.fabio.all;

entity iodevs is
generic(da: daddr := (others=>'0');		-- base address
	addrwidth: natural := 0);		-- range bits
port(	cmd: in iocmd;				-- command
	s, r, w: out std_logic);		-- selected command
end entity;

architecture syn of iodevs is
	signal d: std_logic;			-- address match
begin
	-- match address
	d <= '0' when cmd.d='0'
		else '1' when cmd.da(daddr'length-1 downto addrwidth)
				=da(daddr'length-1 downto addrwidth)
		else '0';
	-- selected operation
	r <= cmd.r and d;
	w <= cmd.w and d;
	s <= (cmd.r or cmd.w) and d;
end syn;

-------------------------------------------------------------------------------
-- select a range of memory addresses
-- default is full range
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;
use work.fabio.all;

entity iomems is
generic(ma: maddr := (others=>'0');		-- base address
	addrwidth: natural := maddr'length);	-- range bits
port(	cmd: in iocmd;				-- command
	s, r, w: out std_logic);		-- selected command
end entity;

architecture syn of iomems is
	signal m: std_logic;			-- address match
begin
	-- match address
	m <= '0' when cmd.m='0'
		else '1' when cmd.ma(maddr'length-1 downto addrwidth)
				=ma(maddr'length-1 downto addrwidth)
		else '0';
	-- selected operation
	r <= cmd.r and m;
	w <= cmd.w and m;
	s <= (cmd.r or cmd.w) and m;
end syn;
