-------------------------------------------------------------------------------
-- simple rom
-- ignores writes
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity rom is
generic(ma: maddr := (others=>'0');		-- base address
	addrwidth: natural := 8;		-- addresses
	data: wordlist := (0 to 255=>X"0000"));	-- contents
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp);			-- continue
end entity;

architecture syn of rom is
	constant addrs: natural := 2**addrwidth;
	constant m: wordlist(0 to addrs-1) := data;

	signal clk: std_logic;
	signal s: std_logic := '0';		-- selected
	signal i: natural range 0 to addrs-1;	-- rom index
	signal rdata: word;			-- rom read
	signal ack: std_logic := '0';		-- [delayed] ack
begin
	clk <= ctl.clk;				-- system clock

	-- address selected
SEL:	component iomems
		generic map(ma=>ma, addrwidth=>addrwidth)
		port map(cmd=>cmd, s=>s);
	i <= to_integer(unsigned(cmd.ma(addrwidth-1 downto 0)));	-- address index

	-- acknowledge	
	rdata <= m(i) when s='1' else (others=>'X');
TAP:	component iolink			-- bus tap
		port map(ack=>s, rdata=>rdata, rest=>rest, rsp=>rsp);

end syn;

