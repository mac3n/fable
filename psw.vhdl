--------------------------------------------------------------------------------
-- the processor status is a device
-- contains alu condition codes and interrupt control
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.fable.all;
use work.fabio.all;

entity psw is
generic(da: daddr := "0000001");		-- device 1
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp; 			-- continue devices
	-- processor state
	cc: in alucc;
	ccw: out std_logic;			-- write CCs
	ccdata: out alucc;			-- write CC data
	isr: in std_logic;			-- executing interrupt
	irq: out std_logic := '0');		-- signalling interupt
end entity;

architecture syn of psw is
	signal clk, rst: std_logic;
	signal s, w, r: std_logic;		-- selected
	signal wdata, rdata: word := (others=>'0');
	signal e, i, p, q: std_logic := '0';	-- interrupt csr
begin
	clk <= ctl.clk;
	rst <= ctl.rst;

	-- device selected
SEL:	component iodevs			-- device select
		generic map(da=>da)		-- 1 addresses
		port map(cmd=>cmd, s=>s, w=>w, r=>r);
	wdata <= cmd.wdata;

	-- acknowledge
TAP:	component iolink			-- bus tap
		port map(ack=>s, rdata=>rdata, rsp=>rsp, rest=>rest);


	rdata(2 downto 0) <= cc;		-- device ccodes
	rdata(3) <= e;				-- interrupt enable
	rdata(4) <= q;				-- interrput queued
	rdata(5) <= i;				-- interrupts active

	ccdata <= wdata(2 downto 0);
	ccw <= w;

	i <= isr;				-- interrupt PC
	q <= p or rest.intr;			-- accumulate interrupts
	process
	begin
		wait until rising_edge(clk);
		p <= q;				-- save interrupts
		if w='1' then
			e <= wdata(3);		-- enable
			p <= q or wdata(4);	-- pend
			irq <= '0';		-- exit interrupt
		elsif i='1' or e='0' then	-- disabled
			null;
		elsif p='1' then		-- take it
			irq <= '1';		-- enter interrupt
			p <= '0';		-- clear pend
		end if;
	end process;

	process
	begin
		wait until rising_edge(clk);
		if w='1' then			-- psw write
			report ">psw"&ht&to_string(wdata)&cr;
		elsif r='1' then		-- psw read
			report "<psw"&ht&to_string(rdata)&cr;
		end if;
	end process;
end syn;
