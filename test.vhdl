--------------------------------------------------------------------------------
-- test rig
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;
use work.fabio.all;
use work.code.all;

entity test is
generic(divisor: natural := 5);			-- default clock divisor
port(	clk: in std_logic;
	rst: in std_logic;			-- reset
	rx: in word;				-- io in
	tx: out word);				-- io out
end entity;

architecture syn of test is

	-- aggregated bus signals
	constant slots: natural := 5;
	signal ctl: ioctl;			-- clocking
	signal cmd: iocmd;			-- broadcast command
	signal ios: iolist(0 to slots);		-- chained responses
	
	-- ICW
	signal irq: std_logic := '0';		-- interrupting
	signal isr: std_logic;			-- interrupted
begin

	ctl.clk <= clk;				-- global clock
	ctl.rst <= rst;				-- synch reset

	-- core cpu & devs
CP:	component cpu
		port map(ctl=>ctl, cmd=>cmd, rest=>ios(0),
			 irq=>irq, isr=>isr);
	-- rom
RO:	component rom
		generic map(ma=>X"0000", addrwidth=>7, data=>code)
		port map(ctl=>ctl, cmd=>cmd, rsp=>ios(0), rest=>ios(1));
	-- ram
RA:	component ram
		generic map(ma=>X"0100", addrwidth=>7)
		port map(ctl=>ctl, cmd=>cmd, rsp=>ios(1), rest=>ios(2));
	-- interrupt control
IC:	component icw
		generic map(da=>"0000001")	-- d1
		port map(ctl=>ctl, cmd=>cmd, rsp=>ios(2), rest=>ios(3),
			isr=>isr, irq=>irq);
		
	-- timer event
CK:	component click
		generic map(da=>"0000100", divisor=>divisor)	-- d4-7
		port map(ctl=>ctl, cmd=>cmd, rsp=>ios(3), rest=>ios(4));
	-- io points
GP:	component gpio
		generic map(da=>"0010000")	-- d10
		port map(ctl=>ctl, cmd=>cmd, rsp=>ios(4), rest=>ios(5),
			rx=>rx, tx=>tx);

	-- end of bus
	ios(slots) <= (intr=>'0', rdata=>(others=>'X'), ack=>'0');
end syn;

--------------------------------------------------------------------------------
-- simulation rig
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity sim is end entity;

architecture sim of sim is
	constant period: time := 20 ns;		-- say, 50MHz
	signal clk: std_logic := '0';		-- clock edges
	signal ticks: word := X"0000";		-- timer
	signal rst: std_logic;

	component test is
	generic(divisor: natural := 50_000);		-- default clock divisor
	port(	clk: in std_logic;
		rst: in std_logic;			-- reset
		rx: in word;				-- io in
		tx: out word);				-- io out
	end component;
begin
	-- unsynthesizable
	clk <= not clk after period/2;
	ticks <= std_logic_vector(unsigned(ticks)+1) when rising_edge(clk);
	rst <= '1' when unsigned(ticks)<4 else '0';

T:	component test
		generic map(divisor=>4)			-- make time pass
		port map(clk=>clk, rst=>rst,
			rx=>ticks);

end sim;
