--------------------------------------------------------------------------------
-- FPGA Able 
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package fable is
	-- 16 bits, word-addressed
	subtype word is std_logic_vector(15 downto 0);	-- 16-bit machine
	subtype daddr is std_logic_vector(6 downto 0);	-- 7-bit device addresses
	subtype maddr is std_logic_vector(15 downto 0);	-- 16-bit memory addresses
	subtype raddr is std_logic_vector(3 downto 0);	-- 4-bit register addresses
	
	type wordlist is array(natural range <>) of word;	-- vector of words

	-- register file with r/post-w/post-inc/post-dec
	component regs is
	port(	clk: in std_logic;
		ra: in raddr;			-- register select
		inc, dec, w: in std_logic;	-- register modify
		wdata: in word;			-- register write
		rdata: out word);		-- register value
	end component;

	-- alu
	subtype aluop is std_logic_vector(3 downto 0);	-- alu operation
	subtype alucc is std_logic_vector(2 downto 0);	-- alu condition codes
	component alu is
	port(	clk: in std_logic;
		a: in std_logic;			-- enable
		op: in aluop;	-- operation
		lhs, rhs: in word;			-- accumulator, source operands
		ci: in std_logic;			-- carry-in
		res: out word;				-- alu result
		cc: out alucc);				-- flags
	end component;

	-- condition code register
	component ccreg is
	port(	clk: in std_logic;
		rst: in std_logic;			-- reset
		rdata: out alucc;			-- current CCs
		isr: in std_logic;			-- executing interrupt
		ccw: in std_logic;			-- write CCs
		wdata: in alucc);			-- updated CCs
	end component;

	-- instruction execution	
	component exec is
	port(	clk: in std_logic;
		rst, irq: in std_logic;			-- reset/interrupt
		hold: in std_logic := '0';		-- hold
		m, d: out std_logic;			-- memory/device op
		r, w: out std_logic;			-- read/write
		da: out daddr;				-- immediate device address
		ra: out raddr;				-- immediate register address
		rinc, rdec, rw: out std_logic;		-- register control
		rdata: in word;				-- fetch/source read
		wdata: out word;			-- source operand
		isr: out std_logic;			-- interrupt active
		a: out std_logic;			-- alu enable
		aop: out aluop;				-- op code
		ar, ac: out std_logic;			-- alu register, cc update
		cc: in alucc);				-- MCZ
	end component;

	-- convert to signal
	function to_stdlogic(b: boolean) return std_logic;
	-- render for debug
	function to_string(w: std_logic_vector) return string;
end fable;

--------------------------------------------------------------------------------
-- conversion utility functions
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

package body fable is
	function to_stdlogic(b: boolean) return std_logic is
	begin
		if b then return '1'; else return '0'; end if;
	end to_stdlogic;
	function to_string(w: std_logic_vector) return string is
		constant hex: string(1 to 16) := "0123456789ABCDEF";
		variable x: std_logic_vector(3 downto 0);
		variable s: string(1 to 1);
	begin
		if w'length<=4 then
			x := X"0";			-- 0-fill
			x(w'length-1 downto 0) := w;	-- right-align
			s(1) := hex(1+to_integer(unsigned(x)));
			return s;
		else
			return to_string(w(w'left downto w'right+4))&to_string(w(w'right+4-1 downto w'right));
		end if;
	end to_string;

end package body;
