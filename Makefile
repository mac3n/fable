.SUFFIXES:	.vhdl .vcd .gwv .a
VOPTIONS=	

STOP=	5000ns				# simulation end time

SRC=	fable.vhdl regs.vhdl alu.vhdl exec.vhdl cc.vhdl cpu.vhdl \
	fabio.vhdl rom.vhdl ram.vhdl gpio.vhdl click.vhdl icw.vhdl \
	test.vhdl

CODE=	code.a

all:	sim

clean:
	ghdl --remove
	-rm *.vcd *.o sim code.vhdl

sim:	$(SRC) code.vhdl
	ghdl -c $(VOPTIONS) $^ -e sim

code.vhdl: $(CODE)
	fas/fas > $@ --name code --size 128 < $^

sim.vcd:	sim
	./$<  --disp-time --vcd=$@ --stop-time=$(STOP)

wave:	sim.vcd
	gtkwave sim.vcd sim.gwv

