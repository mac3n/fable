--------------------------------------------------------------------------------
-- cycle counter
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity dev is
generic(da: daddr := (others=>'0'));		-- base address
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp);			-- continue
end entity;

architecture tick of dev is
	signal clk: std_logic;
	signal s, w: std_logic := '0';		-- select
	signal ticks: word := X"0000";		-- word
begin
	clk<= ctl.clk;				-- system clock

	-- device selected
SEL:	component iodevs			-- device select
		generic map(da=>da)		-- single address
		port map(cmd=>cmd, s=>s, w=>w);

TAP:	component iolink			-- bus tap
		port map(ack=>s, rdata=>ticks, rest=>rest, rsp=>rsp);

	-- ticker
	process
	begin
		wait until rising_edge(clk);
		if ctl.rst='1' then		-- reset
			ticks <= (others=>'0');
		elsif w='1' then		-- write
			ticks <= cmd.wdata;
		else				-- count
			ticks <= std_logic_vector(unsigned(ticks)+1);
		end if;
	end process;
	
end tick;
