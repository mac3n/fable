--------------------------------------------------------------------------------
-- simple ram
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity ram is
generic(ma: maddr := (others=>'0');		-- base address
	addrwidth: natural := 8);		-- addresses
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp);			-- continue
end entity;

architecture syn of ram is
	signal w, r: std_logic := '0';		-- selected
	signal rdata: word;			-- read word
	signal ack: std_logic := '0';		-- [delayed] ack

	component blockram is
	generic(addrwidth: natural := 8);		-- addresses
	port(	clk: in std_logic;
		a: in std_logic_vector(addrwidth-1 downto 0);
		r, w: in std_logic;		-- read/write select
		wdata: in word;			-- register write
		rdata: out word;
		ack: out std_logic);		-- register value
	end component;
begin

	-- address selected
SEL:	component iomems
		generic map(ma=>ma, addrwidth=>addrwidth)
		port map(cmd=>cmd, r =>r, w=>w);
	-- output
TAP:	component iolink			-- bus tap
		port map(ack=>ack, rdata=>rdata, rest=>rest, rsp=>rsp);

BRAM:	component blockram
		generic map(addrwidth=>addrwidth)
		port map(clk=>ctl.clk, 
			a=>cmd.ma(addrwidth-1 downto 0),
			r=>r, rdata=>rdata, w=>w, wdata=>cmd.wdata, ack=>ack);

end syn;

--------------------------------------------------------------------------------
-- infer blockram using an explicitly registered address
-- there's a single cycle latency to register
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;

entity blockram is
generic(addrwidth: natural := 8);		-- addresses
port(	clk: in std_logic;
	a: in std_logic_vector(addrwidth-1 downto 0);
	r, w: in std_logic;		-- read/write select
	wdata: in word;			-- register write
	rdata: out word;
	ack: out std_logic);		-- register value
end entity;

architecture syn of blockram is
	constant addrs: natural := 2**addrwidth;
	signal ar: std_logic_vector(addrwidth-1 downto 0); -- address reg
	signal ai: natural range 0 to addrs-1;	-- address index
	signal m: wordlist(0 to addrs-1);	-- memory
	signal md: word;			-- memory data
	signal mk: std_logic := '0';		-- address ready
begin

	-- address index
	ai <= to_integer(unsigned(ar));
	md <= m(ai) when r='1' else (others=>'X');
	rdata <= md;
	ack <= mk;

	-- register address and ack on next clock
	process
	begin
		wait until rising_edge(clk);
		mk <= '0';
		if (r or w)='1' and mk='0' then
			ar <= a;
			mk <= '1';
		end if;

		if w='1' and mk='1' then
			m(ai) <= wdata;
		end if;
		
	end process;

	-- trace
	process
	begin
		wait until rising_edge(clk);
		if (mk and w)='1' then
			report ">ram"&to_string(a)&ht&to_string(wdata)&cr;
		end if;
		if (mk and r)='1' then
			report "<ram"&to_string(a)&ht&to_string(md)&cr;
		end if;
				
	end process;
end syn;

