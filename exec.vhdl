
--------------------------------------------------------------------------------
-- instruction decode and execution
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;

entity exec is
port(	clk: in std_logic;
	rst, irq: in std_logic;			-- reset/interrupt
	hold: in std_logic := '0';		-- hold
	m, d: out std_logic;			-- memory/device op
	r, w: out std_logic;			-- read/write
	da: out daddr;				-- immediate device address
	ra: out raddr;				-- immediate register address
	rinc, rdec, rw: out std_logic;		-- register control
	rdata: in word;				-- fetch/source read
	wdata: out word;			-- source operand
	isr: out std_logic;			-- interrupt active
	a: out std_logic;			-- alu enable
	aop: out aluop;				-- op code
	ar, ac: out std_logic;			-- alu register, cc update
	cc: in alucc);				-- MCZ
end entity;

architecture syn of exec is
	type stage is (sF, sS, sD);		-- instruction cycle
	signal s: stage := sS;			-- current stage
	signal spc, pc: raddr := X"F";		-- instruction PC
	constant instr0: word := X"CF80";	-- reset instruction

	subtype halfword is std_logic_vector(7 downto 0);	-- halfwords
	signal half: halfword := instr0(7 downto 0);
	signal half2: halfword := instr0(15 downto 8);
	signal idata: word;			-- immediate field

	signal imme: std_logic := '0';		-- immediate source
	signal rince, rdece: std_logic := '0';	-- register side-effects

begin

	rinc <= rince and not hold;		-- auto-inc/dec only once
	rdec <= rdece and not hold;

	-- save source data
	idata(5 downto 0) <= half(5 downto 0);	-- sign extended
	idata(15 downto 6) <= (others=>half(5));
save:	process
	begin
		wait until rising_edge(clk);	-- synchronous process
		case s is
		when sF =>			-- sign-extend immediate field
			wdata <= rdata;
		when sS =>			-- use immediate from instr
			if imme='1' then
				wdata <= idata;
				report "<imm"&ht&to_string(idata)&cr;
			else
				wdata <= rdata;
				report "<src"&ht&to_string(rdata)&cr;
			end if;
		when others => null;
		end case;
	end process;
		
	-- save effective PC at start of instruction
	spc <= "111"&(not irq);			-- chose PC rF/rE for interrupt
	isr <= not pc(0);			-- interrupt state

	-- clock instruction execution when ready
seq:	process
	begin
		wait until rising_edge(clk);	-- synchronous process
		if rst='1' then			-- in reset
			s <= sS;		-- source
			half <= instr0(7 downto 0);
			half2 <= instr0(15 downto 8);
			report "reset"&cr;
		elsif hold='1' then		-- bus wait
			report "hold"&cr;
		else
			case s is
			when sF =>
				s <= sS;	-- start src
				half <= rdata(7 downto 0);	-- src
				half2 <= rdata(15 downto 8);	-- dst
				report ">instr"&ht&to_string(rdata)&cr;				
			when sS =>
				s <= sD;
				half <= half2;	-- start dst
			when sD =>
				s <= sF;
				half <= X"F"&spc; -- fetch from [PC++]
				pc <= spc;	-- save PC index
			when others => s <= sS;	-- out of reset
			end case;
		end if;
	end process;

	-- trace
	process
	begin
		wait until rising_edge(clk);	-- synchronous process
		case s is
		when sF => report "fetch"&cr;
		when sS => report "src"&cr;
		when sD => report "dst"&cr;
		when others => report "start"&cr;
		end case;
	end process;

	
	-- combinatorial instruction halfword decoder
dec:	process(s,half,spc,pc,cc)
	begin
		rince <= '0';			-- clear side-effect controls
		rdece <= '0';
		rw <= '0';
		a <= '0';
		ar <= '0';
		ac <= '0';
		imme <= '0';

		m <= '0';			-- clear bus controls
		d <= '0';
		r <= '0';
		w <= '0';

		da <= (others=>'X');		-- don't cares
		aop <= (others=>'X');
		ra <= (others=>'X');

		-- common dec/reg/mem operands
		
		da <= half(6 downto 0);		-- if dev
		ra <= half(3 downto 0);		-- if reg
		if half(7)='0' then		-- device
			d <= '1';		-- bus
		elsif half(7 downto 6)="11" then	-- register/memory
			m <= half(5);		-- memory
			rince <= half(4);	-- post-inc/dec
		end if;

		-- special case srcs/dsts
		case s is
		when sF =>			-- implicit source M[r(pc)++]
			r <= '1';		-- read
		when sS =>
			r <= '1';		-- read cycle
			if half(7 downto 6)="10" then	-- immediate
				imme <= '1';
				r <= '0';	-- no read
			end if;
		when sD =>
			w <= '1';		-- writing
			if half(7 downto 6)="10" then	-- alu
				a <= '1';
				aop <= half(5 downto 2);	-- opcode
				ar <= '1';	-- write from acc
				ac <= '1';	-- write cc
				ra(3 downto 2) <= "00";	-- acc only
				rw <= '1';	-- write reg
				if half(5 downto 4)="10" then
					rw <= '0';	-- just test
				end if;
			elsif half(7 downto 4)="1100" then -- register write
				rw <= '1';
			elsif half(7 downto 4)="1101" then	-- conditional
				a <= '1';
				aop <= "00"&half(3)&"0";	-- load/add
				ar <= '1';		-- write PC
				ra <= pc;		-- fetched PC
				rince <= '0';
				case half(1 downto 0) is	-- condition
				when "00" => rw <= '1' xor half(2);	-- always
				when "01" => rw <= cc(0) xor half(2);	-- zero
				when "10" => rw <= cc(1) xor half(2);	-- carry
				when "11" => rw <= cc(2) xor half(2);	-- minus
				when others => rw <= '0';
				end case;
			elsif half(7 downto 0)=X"FC" then	-- [rC--]
				rince <= '0';		-- change ++
				rdece <= '1';		-- to --
			end if;
		when others=> null;				-- in reset
		end case;
	end process;

end syn;
