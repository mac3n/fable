use strict;

my $T = 0;						# time
my ($SRC,$DST);						# operations
my $INST = '----';

my @REG = map("____",(0 ... hex('F')));			# full regs
my @R;							# modified regs
my ($MR,$PC);						# fetch PC

while (<>)
{	my ($t,$inst);
	($t,$_) = ($1,$_) if /:(@\d+ns):(.*)/;		# clock
	$inst = $1 if />instr\s(\w+)/;			# exec
	$SRC = $1 if /(<d\w+\s\w+)/;			# dev
	$DST = $1 if /(>d\w+\s\w+)/;			# dev
	$DST = $1 if /(>m\w+\s\w+)/;			# store
	$DST = $1 if /(alu\w\s\w+\(\w\))/;		# alu
	$MR = $1 if /<m(\w+)\s\w+/;			# memory fetch

	$R[hex($1)] = $2 if />r(\w)\s(\w+)/;		# register write
	if (defined $inst)
	{
		print $T," ",$PC,"=>",$INST,":\t";
		print join":",map((defined $_ ? $_ : '    '),@R);
		for (0 .. hex('F')) { $REG[$_] = $R[$_] if defined $R[$_] }
		print "\t",$SRC if $SRC;
		print "\t",$DST if $DST;
		print "\n";
		$T = $t;
		$INST = $inst;
		$PC = hex($MR);
		undef @R;
		undef $SRC;
		undef $DST;
	}
}

