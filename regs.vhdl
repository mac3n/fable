
--------------------------------------------------------------------------------
-- register file with r/post-w/post-inc/post-dec

-- one-clock latency on writes
-- note that XST won't synthesize ram if we provide reset
-- so we leave it up to software to initialize the regs
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;

entity regs is
port(	clk: in std_logic;
	ra: in raddr;			-- register select
	inc, dec, w: in std_logic;	-- register modify
	wdata: in word;			-- register write
	rdata: out word);		-- register value
end entity;

architecture syn of regs is

	component rfile is
	port(	clk: in std_logic;
		ra: in raddr;			-- register select
		w: in std_logic;		-- register modify
		wdata: in word;			-- register write
		rdata: out word);		-- register value
	end component;

	signal rget, rset: word;	-- async read/write values
	signal rw: std_logic;		-- update
begin
RF:	component rfile
		port map(clk=>clk,
			ra=>ra, w=>rw,
			wdata=>rset, rdata=>rget);

	rdata <= rget;			-- asynchronous read
	-- implement inc/dec
	rw <= w or inc or dec;		-- updates
	rset <= std_logic_vector(unsigned(rget)+1) when inc='1'
		else std_logic_vector(unsigned(rget)-1) when dec='1'
		else wdata;
end syn;

--------------------------------------------------------------------------------
-- infer ram
-- this doesn't support a reset
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;

entity rfile is
port(	clk: in std_logic;
	ra: in raddr;			-- register select
	w: in std_logic;		-- register modify
	wdata: in word;			-- register write
	rdata: out word);		-- register value
end entity;

architecture syn of rfile is
	type rarray is array(0 to 15) of word;
	signal rm: rarray := (others=>X"FFFF");
	signal a: natural range 0 to 15;
begin
	a <= to_integer(unsigned(ra));	-- register select
	rdata <= rm(a);			-- asynchronous read
	-- sync update/write
	process
	begin
		wait until rising_edge(clk);	-- synchronous process
		report "<r"&to_string(ra)&ht&to_string(rm(a))&cr;
		if w='1' then
			rm(a) <= wdata;
			report ">r"&to_string(ra)&ht&to_string(wdata)&cr;
		end if;
	end process;
end syn;
