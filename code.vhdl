library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fable.all;
package CODE is
	constant code: wordlist(0 to 128-1) :=
	(
	--	# simple CPU self-test for regression testing
	--	# failure should result in hanging on a read from d0
	0=>	X"CF84",	--	rF = 4		# primitive jump by PC register immediate write
	1=>	X"0000",	--	d0 = d0		# failed to set rF
	--	=		
	--	=
	--	# test taken/not-taken conditional jumps
	--	# we'll need these to check everything else
	4=>	X"D881",	--	j += 1		# relative jump
	5=>	X"0000",	--	d0 = d0		# failed to jump unconditional
	6=>	X"C081",	--	r0 = 1		# register to read
	7=>	X"D8C0",	--	j += r0		# relative 
	8=>	X"0000",	--	d0 = d0		# failed to read register
	9=>	X"8080",	--	r0 := 0		# load and set CC
	10=>	X"D981",	--	jz += 1		# take jump conditional
	11=>	X"0000",	--	d0 = d0		# failed taken Z
	12=>	X"DD81",	--	jnz += 1	# not-taken NZ
	13=>	X"D881",	--	j += 1		# fall-through
	14=>	X"0000",	--	d0 = d0		# failed non-taken NZ
	15=>	X"DF81",	--	jnm += 1	# take NM
	16=>	X"0000",	--	d0 = d0		# failed taken NM
	17=>	X"DB81",	--	jm += 1		# not-taken M
	18=>	X"D881",	--	j += 1		# fall-through
	19=>	X"0000",	--	d0 = d0		# failed non-taken M
	20=>	X"9480",	--	r0 <= 0		# rotate into carry
	21=>	X"DE81",	--	jnc += 1	# taken NC
	22=>	X"0000",	--	d0 = d0		# failed taken NC
	23=>	X"DA81",	--	jc += 1		# not-taken C
	24=>	X"D881",	--	j += 1
	25=>	X"0000",	--	d0 = d0		# failed not-taken C
	--	# register modes
	26=>	X"C0BF",	--	r0 = -1
	27=>	X"C1D0",	--	r1 = r0++	# autoinc
	28=>	X"80C0",	--	r0 := r0	
	29=>	X"D981",	--	jz += 1
	30=>	X"0000",	--	d0 = d0		# failed autoinc
	--	# alu
	--	# assumed load already
	31=>	X"C0BB",	--	r0 = -5
	32=>	X"8885",	--	r0 += 5		# add
	33=>	X"D981",	--	jz += 1
	34=>	X"0000",	--	d0 = d0		# failed add
	35=>	X"C085",	--	r0 = 5
	36=>	X"848A",	--	r0 &= 0A	# and
	37=>	X"D981",	--	jz += 1
	38=>	X"0000",	--	d0 = d0		# failed and
	39=>	X"C085",	--	r0 = 5
	40=>	X"8485",	--	r0 &= 5
	41=>	X"88BB",	--	r0 += -5
	42=>	X"D981",	--	jz += 1
	43=>	X"0000",	--	d0 = d0		# failed and
	44=>	X"C085",	--	r0 = 5
	45=>	X"8C85",	--	r0 -= 5		# sub
	46=>	X"D981",	--	jz += 1
	47=>	X"0000",	--	d0 = d0		# failed sub
	48=>	X"C085",	--	r0 = 5
	49=>	X"9085",	--	r0 ^= 5		# xor
	50=>	X"D981",	--	jz += 1
	51=>	X"0000",	--	d0 = d0		# failed xor
	52=>	X"C085",	--	r0 = 5
	53=>	X"908A",	--	r0 ^= 0A
	54=>	X"8C8F",	--	r0 -= 0F
	55=>	X"D981",	--	jz += 1
	56=>	X"0000",	--	d0 = d0		# failed xor
	57=>	X"C085",	--	r0 = 5
	58=>	X"9480",	--	r0 <= 0		# left rot
	59=>	X"908A",	--	r0 ^= 0A
	60=>	X"D981",	--	jz += 1
	61=>	X"0000",	--	d0 = d0		# failed rot
	62=>	X"C085",	--	r0 = 5
	63=>	X"988F",	--	r0 |= 0F	# or
	64=>	X"908F",	--	r0 ^= 0F
	65=>	X"D981",	--	jz += 1
	66=>	X"0000",	--	d0 = d0		# failed or
	67=>	X"9C80",	--	r0 ~= 0		# load complement
	68=>	X"8881",	--	r0 += 1
	69=>	X"D981",	--	jz += 1
	70=>	X"0000",	--	d0 = d0		# failed complement
	71=>	X"C08A",	--	r0 = 0A
	72=>	X"B085",	--	r0 %= 5		# byte swap with or
	73=>	X"8C85",	--	r0 -= 5
	74=>	X"B080",	--	r0 %= 0
	75=>	X"8C8A",	--	r0 -= 0A
	76=>	X"D981",	--	jz += 1
	77=>	X"0000",	--	d0 = d0		# failed byte swap
	78=>	X"C08A",	--	r0 = 0A
	79=>	X"B4BB",	--	r0 >= -5	# shift right with add
	80=>	X"D981",	--	jz += 1
	81=>	X"0000",	--	d0 = d0		# failed shift right
	82=>	X"C0BE",	--	r0 = -2
	83=>	X"9480",	--	r0 <= 0		# set carry
	84=>	X"C081",	--	r0 = 1
	85=>	X"B881",	--	r0 +c= 1	# add with carry
	86=>	X"8C83",	--	r0 -= 3
	87=>	X"D981",	--	jz += 1
	88=>	X"0000",	--	d0 = d0		# failed add with carry
	89=>	X"C0BE",	--	r0 = -2
	90=>	X"9480",	--	r0 <= 0		# set carry
	91=>	X"C082",	--	r0 = 2
	92=>	X"BC81",	--	r0 -c= 1
	93=>	X"D981",	--	jz += 1
	94=>	X"0000",	--	d0 = d0		# failed sub with carry
	95=>	X"CF80",	--	rF = 0		# repeat
	others=>	(others=>'U'));
end package;
