%{

#include "fas.h"

int yylval;
static int yylex()	{ return yylval = srcChar(); }

#define yyerror(MSG)

static int Sign16(int n)	{ return n&0x8000 ? n|(~0<<16) : n&0xFFFF; }

static int Range(int n, int lo, int hi)
{	if (n<lo ||  hi<=n)
		Error("%X not in range  %X ... %X\n",n,lo,hi);
	return n;
}

static int Width(int n, int bits) { return Range(n,0,1<<bits); }

static int Subop(int n, int mask)
{	if (n&~mask)
		Error("not an operator\n");
	return n;
}
%}

%start code

%%
/* lexical classes */
dec1	: '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';
hex1	: 'A' | 'B' | 'C' | 'D' | 'E' | 'F';

sym1	: 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M'
	| 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z';
sym2	: sym1 | dec1;

/* symbols begin with letter */
sym	: sym1				{ symAdd($1); };
sym	: sym sym2			{ symAdd($2); };

/* numbers are hex but begin with digit */
dec	: dec1				{ $$ = $1-'0'; };
hex	: hex1				{ $$ = 10+$1-'A'; };
hex	: dec;

num	: dec;
num	: num hex			{ $$ = ($1<<4)+$2; };
expr	: num;
expr	: '-' num			{ $$ = -$2; }
word	: '=' expr			{ objWord(0xFFFF&Range(Sign16($2),-32768,32767)); };

dev	: 'D' hex			{ $$ = $2; };
dev	: dev hex			{ $$ = ($1<<4)+$2; };

reg	: 'R' hex			{ $$ = 0xC0|$2; };

ind	: reg;
ind	: reg '+' '+'			{ $$ = $1|0x10; };
ind	: reg '-' '-'			{ $$ = 0xD0|Range(0xF&$1,12,13); };
mem	: '[' ind ']'			{ $$ = 0x20|$2; };

imm	: expr				{ $$ = 0x80|(0x3F&Range(Sign16($1),-32,32)); }

src	: dev | ind | mem | imm;
src	: error				{ Error("not a src\n"); };

dst	: dev | reg | mem;
dst	: error				{ Error("not a dst\n"); };
word	: dst '=' src 			{ objWord(($1<<8)|$3); };

acc	: reg				{ $$ = 0x80|Width(0xF&$1,2); };
aop	: ':'				{ $$ = 0x00; }	/* load */
	| '&'				{ $$ = 0x04; }	/* and */
	| '+'				{ $$ = 0x08; }	/* add */
	| '-'				{ $$ = 0x0C; }	/* sub */
	| '^'				{ $$ = 0x10; }	/* xor */
	| '<'				{ $$ = 0x14; }	/* left rotate */
	| '|'				{ $$ = 0x18; }	/* or */
	| '~'				{ $$ = 0x1C; }	/* complement */
	| '%'				{ $$ = 0x30; }	/* byteswap or */
	| '>'				{ $$ = 0x34; }	/* right shift add */
	| '+' 'C'			{ $$ = 0x38; }	/* add w/ carry */
	| '-' 'C'			{ $$ = 0x3C; }	/* sub w carry */
	| error				{ Error("not an accumulator op\n"); }
	;
word	: acc aop '=' src		{ objWord((($1|$2)<<8)|$4); };

top	: aop				{ $$ = 0x20|Subop($1,0x0C); };
word	: acc top '?' src		{ objWord((($1|$2)<<8)|$4); };

jmp	: 'J' cc			{ $$ = $2; }
	| 'J' 'N' cc			{ $$ = 0x4|$3; }
	;
cc	: 'M'				{ $$ = 0x3; }	/* if minus */
	| 'C'				{ $$ = 0x2; }	/* if carry */
	| 'Z'				{ $$ = 0x1; }	/* if zero */
	| 				{ $$ = 0x0; }	/* always */
	;
jop	: aop				{ $$ = 0xD0|Subop($1,0x8); };
word	: jmp jop '=' src		{ objWord((($1|$2)<<8)|$4); };

word	: '=' sym			{ symRef(); };
word	: '='				{ objLoc++; };
word	: error				{ Error("not an instruction\n"); };

loc	: ':' sym			{ symDef(); };
loc	: '@' num			{ objLoc = $2; };
loc	:;

line	: word | loc | error;
code	: code line '\n'		{ srcList(); };
code	:;
%%
