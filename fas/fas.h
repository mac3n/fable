extern int srcLine;				/* source location */
void	srcList(void);				/* output the source line */
int	srcChar(void);				/* get the next significant char */

int	symAdd(int c);				/* add char to sym */
void	symDef(void), symRef(void);		/* add symbol def/ref */

extern int objLoc;				/* object loc (address) */
void	objWord(int w);				/* output word */

extern int yyparse(void);			/* use significant chars */

void	Error(const char *fmt, ...);		/* report error */
