/* lexer */
#include "fas.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

int srcErrs = 0;				/* accumulate errors */
void
Error(const char *fmt, ...)
{	va_list ap;
	va_start(ap,fmt);
	if (srcLine)
		fprintf(stderr, "line %d: ",srcLine);
	vfprintf(stderr,fmt,ap);
	va_end(ap);
	++srcErrs;
}

/*________________________________________________
 * some vhdl layout
 */
static void
vhdlProlog(const char *name, int loc, const char *size)
{	printf("library IEEE;\nuse IEEE.STD_LOGIC_1164.ALL;\nuse work.fable.all;\n");
	printf(	"package %s is\n",name);
	printf("\tconstant code: wordlist(%d to %s%+d) :=\n",loc,size,loc-1);
	printf("\t(\n");
}

typedef const char * const vhdlfmt;		/* format strings for output */
static vhdlfmt
	vhdlWord	= "\t%d=>\tX\"%04X\",",	/* loc and data */
	vhdlNOP		= "\t\t\t",		/* no data word for ref/def */
	vhdlFix		= "\t%d=>\tX\"%04X\",\t--\t@%s\n", /* fixup loc ref */
	vhdlNote	= "\t--\t%s\n";		/* append source as comment */

static void
vhdlEpilog(const char *fill)
{	printf("\tothers=>\t%s);\n",fill);
	printf("end package;\n");
}

/*________________________________________________
 * expandable text buffer
 */
struct textstr { char *text; int off; int size; };
static char
textAdd(struct textstr *t, char c)
{	if (t->size<=t->off)			/* expand buffer */
		t->text = realloc(t->text,t->size += 512);
	return t->text[t->off++] = c;		/* save char */
}

/* make string and reset */
static char
*textOut(struct textstr *t)
{	textAdd(t,'\0');			/* stringify */
	t->off = 0;				/* reset */
	return t->text;
}

/*________________________________________________
 * save source characters for listing
 * and deliver significant characters for parsing
 */
static struct textstr src;			/* source line */

/* output the source line */
int srcLine = 1;				/* source location */
void
srcList()
{	if (src.off)				/* nonempty */
		printf(vhdlNote,textOut(&src));	/* out */
	srcLine++;				/* count lines */
}

/* skip over comment text and return EOL */
static int
srcSkip()
{	for (;;)
	{	int c = getchar();
		if (c==EOF || c=='\n')		/* EOL */
			return '\n';
		textAdd(&src,c);		/* save as text */
	}
}

/* get the next significant char */
int
srcChar()
{	for (;;)
	{	int c = getchar();
		if (c==EOF && !src.off)		/* EOF at EOL */
			return c;
		if (c==EOF || c=='\n')		/* EOL */
			return '\n';
		textAdd(&src,c);		/* save as text */
		if (c=='#')			/* start comment */
			return srcSkip();	/* skip to EOL */
		if (' '<c)			/* skip white */
			return toupper(c);	/* ignore case */
	}
}

/*________________________________________________
 * labels use a list of definitions and a list of references
 * references are fixed up at the end of asembly
 */
struct symloc { char *sym; int loc; };		/* symbol def/ref */
struct symtbl { struct symloc *list; int size; };	/* symbol list */

/* append to table */
static void
tblAdd(struct symtbl *l, char *sym, int loc)
{	int i = l->size++;			/* grow */
	l->list = realloc(l->list,(i+1)*sizeof *l->list);
	l->list[i].sym = strdup(sym);		/* copy sym */
	l->list[i].loc = loc;
}

/* sort table by symbol */
static int tblCmp(const void *a, const void *b)
{	const struct symloc *p = a, *q = b;
	return strcmp(p->sym,q->sym);
}
static void tblSort(struct symtbl *l)	{ qsort(l->list,l->size,sizeof *l->list,tblCmp); }

/* Check for inconsistent locs in sorted list */
static void
tblUniq(struct symtbl *l)
{	int i;
	for (i = l->size-1; 0<i; --i)
	{ 	struct symloc *a = &l->list[i-1], *b = &l->list[i];
		if (!strcmp(a->sym,b->sym) && (a->loc!=b->loc))
			Error("redefined %s\n",b->sym);
	}		
}

static struct textstr symtext;			/* symbol name */
int symAdd(int c)	{ return textAdd(&symtext,c); }

/* Tables of symbols and fixup locs */
static struct symtbl defs, refs;		/* tables */
void symDef()	{ printf(vhdlNOP); tblAdd(&defs,textOut(&symtext),objLoc); }
void symRef()	{ printf(vhdlNOP); tblAdd(&refs,textOut(&symtext),objLoc++); }
static void symLoc(char *sym, int loc)	{ tblAdd(&defs,sym,loc); }
static void	symErr(struct symloc *r)	{ Error("undefined %s at %d\n",r->sym,r->loc); }

/*________________________________________________
 * output a VHDL array
 * code is in order with references appended
 */
int objLoc = 0;					/* object loc (address) */
void objWord(int w)	{ printf(vhdlWord,objLoc++,w&0xFFFF); }

/* match refs to defs and output fixup */
static void
objFix()
{	int d = 0, r = 0;			/* scan lists i*/
	tblSort(&defs);				/* sort defs/refs by symbol */
	tblUniq(&defs);
	tblSort(&refs);
	if (refs.size)
		printf(vhdlNote,"fixup");
	while (r<refs.size && d<defs.size)	/* collate */
	{	struct symloc *def = &defs.list[d], *ref = &refs.list[r];
		int c = strcmp(def->sym,ref->sym);
		if (!c)				/* match */
		{	printf(vhdlFix,ref->loc,def->loc,ref->sym);
			++r;
		}
		else if (c<0)
			++d;
		else				/* missing def */
		{	symErr(ref);
			++r;
		}
	}
	while (r<refs.size)
		symErr(&refs.list[r++]);
}

/*________________________________________________
 * wrap output in VHDL package (the basic external unit)
 */
int
main(int argc, char *argv[])
{	char *name = "CODE";
	char *size = "128";
	char *fill = "(others=>'U')";
	int a;

	for (a = 1; a<argc; ++a)
	{	char *arg = argv[a];
		if (!strcmp(arg,"--name"))
			name = argv[++a];
		else if (!strcmp(arg,"--size"))
			size = argv[++a];
		else if (!strcmp(arg,"--fill"))
			fill = argv[++a];
		else if (!strcmp(arg,"--loc"))
			objLoc = atoi(argv[++a]);
		else if (!strcmp(arg, "--def"))
		{	symLoc(argv[a+1],atoi(argv[a+2]));
			a += 2;
		}
		else
			Error("no option %s\n",arg);
	}

	vhdlProlog(name,objLoc,size);
	yyparse();				/* source code */
	objFix();				/* fixup */
	vhdlEpilog(fill);

	return -srcErrs;
}
