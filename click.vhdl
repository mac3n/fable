
--------------------------------------------------------------------------------
-- ticker and clicker
-- ticker runs at programmable rate
-- clicks bits can signal regular intervals
-- click writeback clears bits

-- 0 r/w ticks
-- 1 r/w ticks divisor
-- 2 r clicks accumulated ticks carry bits
-- 2 w clear click bits
-- 3 click interrupt enables
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity click is
generic(da: daddr := (others=>'0');		-- base address
	divisor: natural := 50_000);		-- default divisor
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp);			-- continue
end entity;

architecture syn of click is
	signal clk, rst: std_logic;
	signal s, w, r: std_logic;		-- selected
	signal a: std_logic_vector(1 downto 0);	-- function
	signal intr: std_logic := '0';		-- interrupt
	signal wdata, rdata: word := (others=>'0');

	-- ticks
	signal ticks, ticks1, carry: word := (others=>'0');
	-- clock divisor
	constant div: word := std_logic_vector(to_unsigned(divisor-1,word'length)); 
	signal wrap: word := div; 		-- reload
	signal decr, decr1: std_logic_vector(word'length downto 0) := (others=>'0');
	signal incr: std_logic := '0';
	-- clicks
	signal clicks: word := (others=>'0');
	signal enable: word := (others=>'0');
begin
	clk <= ctl.clk;
	rst <= ctl.rst;
	
	a <= cmd.da(1 downto 0) when s='1' else "00";
	wdata <= cmd.wdata;
	with a select rdata <= ticks when "00",
		wrap when "01",
		clicks when "10",
		enable when "11",
		(others=>'0') when others;

	-- device selected
SEL:	component iodevs			-- device select
		generic map(da=>da, addrwidth=>2)	-- 4 address
		port map(cmd=>cmd, s=>s, w=>w, r=>r);

	-- acknowledge
TAP:	component iolink			-- bus tap
		port map(intr=>intr, ack=>s, rdata=>rdata, rest=>rest, rsp=>rsp);

	-- ticks
	ticks1 <= std_logic_vector(unsigned(ticks)+1) when incr='1' else ticks;
	carry <= ticks1 xor ticks;
	process
	begin
		wait until rising_edge(clk);
		if rst='1' then			-- reset
			ticks <= (others=>'0');
		elsif w='1' and a="00" then	-- write ticks
			ticks <= wdata;
		else				-- count
			ticks <= ticks1;			
		end if;
	end process;

	-- clock divisor
	decr1 <= std_logic_vector(unsigned(decr)-1);
	incr <= decr1(decr1'left);	-- wrapt
	process
	begin
		wait until rising_edge(clk);
		if rst='1' then			-- reset
			wrap <= div;
			decr <= (others=>'0');
		elsif w='1' and a="01" then	-- write divisor
			wrap <= wdata;
		elsif incr='0' then
			decr <= decr1;		-- count
		else
			decr(wrap'left downto 0) <= wrap;	-- wrap
		end if;
	end process;

	-- clicker
	process
	begin
		wait until rising_edge(clk);
		if rst='1' then			-- reset
			clicks <= (others=>'0');
		elsif w='1' and a="10" then	-- clear clicks
			clicks <= clicks and not wdata;
		else
			clicks <= clicks or carry;
		end if;
	end process;

	-- enables
	intr <= '1' when unsigned(clicks and enable)/=0 else '0';
	process
	begin
		wait until rising_edge(clk);
		if rst='1' then			-- reset
			enable <= (others=>'0');
		elsif w='1' and a="11" then	-- clear clicks
			enable <= wdata;
		end if;
	end process;
	
end syn;
