
--------------------------------------------------------------------------------
-- read-modify-write RAM
-- note that XST won't synthesize ram if we provide reset
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.fable.all;
use work.fabio.all;

entity rmwram is
generic(addrwidth: natural := 8);	-- address bits
port(	clk: in std_logic;
	s: in std_logic;		-- select
	ma: in maddr;			-- address
	mask: in word;			-- write mask
	wdata: in word;			-- register write
	rdata: out word);		-- register value
end entity;

architecture syn of rmwram is
	constant addrs: natural := 2**addrwidth;
	signal m: wordlist(0 to addrs-1);	-- RAM file
	signal i: natural range 0 to addrs-1;	-- rom index

	signal xdata: word;			-- read
	signal mdata: word;			-- writeback data
begin
	-- address index
	i <= to_integer(unsigned(ma(addrwidth-1 downto 0))) when s='1' else 0;
	-- read
	xdata <= m(i) when s='1' else (others=>'X');
	rdata <= xdata;
	-- mixed read and writeback
	mdata <= (xdata and not mask) or (wdata and mask);
	
	process
	begin
		wait until rising_edge(clk);
		if s='1' then
			m(i) <= mdata;
		end if;
	end process;

end syn;

