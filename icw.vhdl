--------------------------------------------------------------------------------
-- the processor status is a device
-- contains alu condition codes and interrupt control
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.fable.all;
use work.fabio.all;

entity icw is
generic(da: daddr := "0000001");		-- device 1
port(	ctl: in ioctl;				-- clocking
	cmd: in iocmd;				-- command
	rsp: out iorsp;				-- output
	rest: in iorsp; 			-- continue devices
	-- interrupt control
	isr: in std_logic;			-- executing interrupt
	irq: out std_logic := '0');		-- signalling interupt
end entity;

architecture syn of icw is
	signal clk, rst: std_logic;
	signal s, w, r: std_logic;		-- selected
	signal wdata, rdata: word := (others=>'0');
	signal rirq: std_logic;				-- request

	signal i: std_logic := '0';		-- in interrupt handler
	signal p: std_logic := '0';		-- queued interrupts
	signal q: std_logic := '0';		-- queued interrupts
	signal e: std_logic := '0';		-- interrupt enable
	
begin
	clk <= ctl.clk;
	rst <= ctl.rst;

	-- device selected
SEL:	component iodevs			-- device select
		generic map(da=>da)		-- 1 addresses
		port map(cmd=>cmd, s=>s, w=>w, r=>r);
	wdata <= cmd.wdata;

	-- acknowledge
TAP:	component iolink			-- bus tap
		port map(ack=>s, rdata=>rdata, rsp=>rsp, rest=>rest);

	-- register read
	rdata(0) <= e;
	rdata(1) <= i;
	rdata(2) <= p;

	p <= q or rest.intr;			-- accumulate

	rirq <= p and e and not i;		-- request interrupt
	irq <= rirq or i;			-- hold request


	-- handle CC writes and interrupt entry
	process
	begin
		wait until rising_edge(clk);
		q <= p;
		if rst='1' then
			e <= '0';
			i <= '0';
			q <= '0';
		elsif w='1' then
			e <= wdata(0);
			i <= wdata(1);
			q <= wdata(2);
		elsif (rirq and isr)='1' then
			-- enter interrupt
			report "interrupt"&cr;
			i <= '1';
			q <= '0';
		end if;
	end process;

	process
	begin
		wait until rising_edge(clk);
		if w='1' then			-- psw write
			report ">icw"&ht&to_string(wdata)&cr;
		elsif r='1' then		-- psw read
			report "<icw"&ht&to_string(rdata)&cr;
		end if;
	end process;
end syn;

