# fable

FPGA implementation of New England Digital Able processor.

This project was built with Xilinx ISE and ran on a digilent Spartan3 development board.
It was tested under GHDL.

There may be issues with the RAM/ROM and register modules, as Xilinx deprecated asynchronous block RAM reads.
